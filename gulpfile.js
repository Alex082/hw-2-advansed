const { src, dest, watch, parallel, series } = require("gulp");

const scss = require("gulp-sass")(require("sass"));
const concat = require("gulp-concat");
const uglify = require("gulp-uglify-es").default;
const browserSync = require("browser-sync").create();
const autoprefixer = require("gulp-autoprefixer");
const clean = require("gulp-clean");

// для картинок
const avif = require("gulp-avif");
const webp = require("gulp-webp");
const imagemin = require("gulp-imagemin");
const svgSprite = require("gulp-svg-sprite");

const newer = require("gulp-newer");
const fonter = require("gulp-fonter");
const ttf2woff2 = require("gulp-ttf2woff2");
const include = require("gulp-include");

function pages() {
  return src("src/pages/*.html")
    .pipe(
      include({
        includePaths: "src/components",
      })
    )
    .pipe(dest("src"))
    .pipe(browserSync.stream());
}

function fonts() {
  return src("src/fonts/src/*.*")
    .pipe(
      fonter({
        formats: ["woff", "ttf"],
      })
    )
    .pipe(src("src/fonts/*.ttf"))
    .pipe(ttf2woff2())
    .pipe(dest("src/fonts"));
}

// для картинок
function images() {
  return src(["src/img/src-img/*.*", "!src/img/src/*.svg"])
    .pipe(newer("src/img"))
    .pipe(avif({ quality: 50 }))

    .pipe(src("src/img/src-img/*.*"))
    .pipe(newer("src/img"))
    .pipe(webp())

    .pipe(src("src/img/src-img/*.*"))
    .pipe(newer("src/img"))
    .pipe(imagemin())

    .pipe(dest("src/img"));
}

function sprite() {
  return src("src/img/*.svg")
    .pipe(
      svgSprite({
        mode: {
          stack: {
            sprite: "../sprite.svg",
            example: true,
          },
        },
      })
    )
    .pipe(dest("src/img"));
}

function scripts() {
  return src(["src/js/main.js"])
    .pipe(concat("main.min.js"))
    .pipe(uglify())
    .pipe(dest("src/js/"))
    .pipe(browserSync.stream());
}

function styles() {
  return (
    src("src/scss/style.scss")
      .pipe(autoprefixer({ overrideBrowserslist: ["last 10 version"] }))
      // .pipe(concat("style.css"))
      .pipe(concat("style.min.css"))
      .pipe(scss({ outputStyle: "compressed" }))
      .pipe(dest("src/css"))
      .pipe(browserSync.stream())
  );
}

function watching() {
  browserSync.init({
    server: {
      baseDir: "src/",
    },
  });
  watch(["src/scss/**/*.scss"], styles);
  watch(["src/img/src-img"], images);
  watch(["src/js/main.js"], scripts);
  watch(["src/components/*", "src/pages/*"], pages);
  watch(["src/*.html"]).on("change", browserSync.reload);
}

function cleanDist() {
  return src("dist").pipe(clean());
}

function building() {
  return src(
    [
      "src/css/style.min.css",
      // "src/css/style.css",
      "src/img/*.*",
      "src/img/*.svg",
      "src/img/sprite.svg",
      "src/fonts/*.*",
      "src/js/main.min.js",
      "src/**/*.html",
    ],
    { base: "src" }
  ).pipe(dest("dist"));
}

exports.styles = styles;
exports.images = images;
exports.fonts = fonts;
exports.pages = pages;
exports.building = building;
exports.sprite = sprite;
exports.scripts = scripts;
exports.watching = watching;

exports.build = series(cleanDist, building);
exports.default = parallel(styles, scripts, pages, watching);
