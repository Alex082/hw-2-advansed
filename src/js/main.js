const navigationIcon = document.querySelector(".navigation__icon");
const navigationList = document.querySelector(".navigation__list");

navigationIcon.addEventListener("click", () => {
  if (navigationIcon.innerText === "menu") {
    navigationIcon.innerText = "close";
  } else {
    navigationIcon.innerText = "menu";
  }
  navigationList.classList.toggle("invisible");
});

if (window.matchMedia("(min-width: 768px)").matches) {
  navigationList.classList.remove("invisible");
}

if (window.matchMedia("(min-width: 1200px)").matches) {
  navigationList.classList.remove("invisible");
}

navigationIcon.addEventListener("click", () => {
  const screenWidth = window.innerWidth;
  if (screenWidth <= 320) {
    navigationList.classList.toggle("active");
  }
});

const navigationItems = document.querySelectorAll(".navigation__item");

navigationItems.forEach((item) => {
  item.addEventListener("click", () => {
    navigationItems.forEach((item) => {
      item.classList.remove("active");
    });

    item.classList.add("active");
  });
});
